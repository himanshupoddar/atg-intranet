from django.db import models

# Create your models here.
class offerlink(models.Model):
    offer_id = models.AutoField(primary_key=True)
    email = models.EmailField(max_length=70,blank=True)
    department = models.CharField(max_length=50)
    offer_sent_by = models.CharField(max_length=50)
    link= models.CharField(max_length=150)
    assigned_time=models.CharField(max_length=200)
    expired_time=models.CharField(max_length=200)