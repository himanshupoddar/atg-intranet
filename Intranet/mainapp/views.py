from django.shortcuts import render,redirect
import requests
from django.contrib import messages
import string
from django.core.mail import EmailMultiAlternatives
from mainapp.models import offerlink
import random
import time
from django.template.loader import render_to_string
 from django.contrib.auth import get_user_model

# Create your views here.
def homepage(request):
    return render(request, 'mainapp/homepage.html')
def Leave(request):
    return render(request, 'mainapp/Leave.html')
def Heirarchy(request):
    return render(request, 'mainapp/Heirarchy.html')
def Payroll(request):
    return render(request, 'mainapp/Payroll.html')
def Documentation(request):
    return render(request, 'mainapp/Documentation.html')
def Onboard(request):
    msg=""
    try:
        if request.POST:
            login_data = request.POST.dict()
            user = login_data.get("username")
            password = login_data.get("password")
            name = login_data.get("name")
            email = login_data.get("email")
            option=login_data.get("opt")
            url="http://mantis.atg.party/api/rest/users/"
            header={
            'Authorization':'mhVBa0ZRB7CCOdd2AGF2RuULv8LCKSp8',
            'Content-Type':'application/json'
            }
            payload=f"{{\n  \"user\":\"{user}\",\n \"pass\":\"{password}\",\n \"name\":\"{name}\",\n \"email\":\"{email}\",\n \"option\":\"{option}\",\n  \"enabled\": false}}"
            x=requests.post(url,headers=header,data=payload,timeout=100000)
            '''if x.status_code==201:
                database=get_user_model()
                database.create_superuser(user,email,password)
                database.save()
                msg="User Successfully Created!!"
            else:
                msg=x.json()
            '''
            msg=x.json()



            
    except Exception as exp:
        msg=exp
    return render(request, 'mainapp/Onboard.html',{'msg':msg})
def Track(request):
    return render(request, 'mainapp/Track.html')
def Delete(request):
    msg=""
    try:
        if request.POST:
            data=request.POST.dict()
            user_id=data.get("User_ID")
            url = f'http://mantis.atg.party/api/rest/users/{user_id}'
            headers = {
              'Authorization': 'mhVBa0ZRB7CCOdd2AGF2RuULv8LCKSp8'
            }
            response = requests.request('DELETE', url, headers = headers,timeout=20000)
            msg=response.json()

    except Exception as exp:
        msg="Successfully Deleted"
    return render(request, 'mainapp/delete.html',{'msg':msg})
def Bug(request):
    msg=""

    try:
        if request.POST:
            data=request.POST.dict()
            bug_id=data.get("Bug_ID")
            url=f'http://mantis.atg.party/api/rest/issues/{bug_id}'
            headers={
            'Authorization': 'mhVBa0ZRB7CCOdd2AGF2RuULv8LCKSp8'
            }
            response = requests.request('GET', url, headers = headers, timeout=20000)
            msg=response.json()
    except Exception as exp:
        msg=exp

    return render(request, 'mainapp/bug.html',{'msg':msg})


def mail(request):
	
	global key,email_id,option_id
	msg=""
	try:

		if request.POST:
			key="".join([random.SystemRandom().choice(string.ascii_letters + string.digits + string.punctuation)for _ in range(50)])
			data=request.POST.dict()
			email_id=data.get("email")
			option_id=data.get("opt")
			html_content = render_to_string('mainapp/htmlmsg.html',{'key':key})
	           	
			msg=EmailMultiAlternatives('ATG',"Hello\n we are from atg.dev.party\n Click on this link to get offer","",[email_id])
			msg.attach_alternative(html_content,"text/html")
			msg.send()
			db = offerlink.objects.create(email=email_id,department=option_id,offer_sent_by='sparshjain',link=key,assigned_time=time.time(),expired_time=(time.time()+3*82400))
			db.save()
			msg="Mail has been sent!!!\n Check your Inbox!!"
	except Exception as exp:
		msg=exp
	return render(request, 'mainapp/offer.html',{'secret':msg})

def dummy(request):
	try:
		db=offerlink.objects.filter(email=email_id,department=option_id)
		dbfresh=db[len(db)-1]
		msg="Your offer is expired!!!!"
		if time.time()>=float(dbfresh.expired_time):
			return render(request,'mainapp/greeting.html',{'msg':msg})
			
		else:
			return render(request,'mainapp/dummy.html',{'user':email_id.split('@')[0]})
			
	except Exception as exp:
		pass
def nda(request):
	return render(request,'mainapp/nda.html',{'user':email_id.split('@')[0]})
		
	
'''
def create(request):
   
        url="http://mantis.atg.party/api/rest/users/"
        header={
        'Authorization':'mhVBa0ZRB7CCOdd2AGF2RuULv8LCKSp8',
        'Content-Type':'application/json'
        }
        payload = "{\n  \"username\": \"test_user_21\",\n  \"password\": \"p@ssw0rd\",\n  \"real_name\": \"Victor Boctor\",\n  \"email\": \"jecteen@gmail.com\",\n  \"enabled\": true,\n  \"protected\": false\n}" 
        x=requests.post(url,headers=header,data=payload,timeout=100000)
        print(x.status_code)
        print(payload)
        return render(request,'mainapp/Onboard.html')
'''
